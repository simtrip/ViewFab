﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field, AllowMultiple =false)]
public class ViewFab : PropertyAttribute
{
    public GameObject prefab;
}

[CustomPropertyDrawer(typeof(ViewFab))]
public class ViewFabDrawer : PropertyDrawer
{
    private UnityEngine.Object _reference;
    private UnityEngine.Object _prefabObject;
    private GameObject _instance;
    private Transform _transform;
    private ViewFabCleanupBehaviour _cleanup;
    private bool _previewEnabled = true;
    private bool _prefabExists = false;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //First lets make the GUI for the object field
        EditorGUI.BeginProperty(position, label, property);

        //This it the rect to display the regular object field
        Rect fieldRect = new Rect(position.x, position.y, position.width, position.height * 0.5f);
        //Below it, we're going to make a checkbox. It's the same height but Y offset by the other one's height
        Rect toggleRect = new Rect(fieldRect.x, fieldRect.y + fieldRect.height, fieldRect.width, fieldRect.height);

        //Render The field
        EditorGUI.ObjectField(fieldRect, property, label);

        //We're not going to render the toggle until we know we've set our field correctly.

        //Are we in freaking PLAY MODE?!
        if (Application.isPlaying)
            return;

        //The reference should be a GameObject.
        _reference = property.objectReferenceValue as GameObject;

        //Dont do anything if we didnt set the right type or set null
        if (_reference == null)
            return;

        //Did we actually set a prefab or something from scene?
        _prefabObject = PrefabUtility.GetPrefabObject(_reference);
        if (_prefabObject == null)
            return;
        
        //Try and get component and transform of the object we're working on
        var component = property.serializedObject.targetObject as Component;
        _transform = component.transform;

        //Maybe that didn't work, maybe we referenced some other Object
        if (component == null || _transform == null)
        {
            return;
        }

        //Here's a horrific hack that I unfortunately need.
        //Attach our cleanup behaviour, if it's not already attached.
        _cleanup = component.GetComponent<ViewFabCleanupBehaviour>();
        if (_cleanup == null)
            _cleanup = component.gameObject.AddComponent<ViewFabCleanupBehaviour>();



        EditorGUI.BeginChangeCheck();

        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = indent + 1;
        _previewEnabled = EditorGUI.Toggle(toggleRect, "Preview", _previewEnabled);


        if (EditorGUI.EndChangeCheck())
        {
            //Respond to toggle status now
            if (_previewEnabled)
            {
                CreatePrefab();
            }
            else
            {
                DestroyPrefab();
            }                
        }
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        //We need enough for two regular fields.
        return EditorGUI.GetPropertyHeight(property) * 2.0f;
    }


    private void CreatePrefab()
    {
        //Make a new one
        _instance = PrefabUtility.InstantiatePrefab(_reference) as GameObject;
        _instance.name = _reference.name;
        _instance.transform.SetParent(_transform);
        _instance.transform.localPosition = Vector3.zero;
        //Register it for cleanupp
        _cleanup.RegisterDeletion(_instance.gameObject.GetInstanceID());

        _prefabExists = true;
    }

    private void DestroyPrefab()
    {
        var existingName = _transform.Find(_reference.name);
        if (existingName != null)
        {
            //Is it actually the same prefab?
            if (PrefabUtility.GetPrefabObject(_reference).GetInstanceID() == _prefabObject.GetInstanceID())
            {
                //We don't need to clean  it up if we're deleting it ourselves.
                _cleanup.UnregisterDeletion(existingName.gameObject.GetInstanceID());
                GameObject.DestroyImmediate(existingName.gameObject);
                _prefabExists = false;
            }
        }
    }
}