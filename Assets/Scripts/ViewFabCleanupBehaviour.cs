﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class ViewFabCleanupBehaviour : MonoBehaviour
{
    //TODO: A Dictionary would be better but need to stop them being cleared in Play mode due to 
    [SerializeField] [HideInInspector] private List<int> _deletionIDs = new List<int>();

    public void RegisterDeletion(int instanceId)
    {
        _deletionIDs.Add(instanceId);
    }

    public void UnregisterDeletion(int instanceId)
    {
        _deletionIDs.Remove(instanceId);
    }

    private void Awake()
    {
        if (Application.isPlaying)
        {
            foreach (int id in _deletionIDs)
            {
                DestroyImmediate(EditorUtility.InstanceIDToObject(id));
            }

            DestroyImmediate(this);
        }
    }
}
