## Overview
ViewFab is a quick solution to allow you to create working copies of prefabs from GameObject script references while inside the Editor. This allows you to easily work on prefabs that you would otherwise intend to be instantiated at runtime without manually dragging it in from the Project view and remembering to clean it up before you enter play mode.

## Usage
On any `MonoBehaviour` script, simply add the `[ViewFab]` attribute before an Object reference field. This will show a `Previewable` checkbox under the reference within the Inspector, which can be ticked to spawn the prefab as a child of the script in editor mode. The prefab is instantiated with its reference intact, so you're free to work on the prefab and apply your changes as if you had dragged it in. Once Play mode is entered, all prefabs spawned this way will be destroyed.


### Example
```csharp
[ViewFab] [SerializeField] private GameObject _prefabReference;
```

## Notes
* This is a very work-in-progress script and the code is in need of cleaning up.
* In order to clean up prefab references at runtime, the PropertyDrawer has to add an additional behaviour to the script. This script clears up the prefabs during `Awake` and then removes itself.